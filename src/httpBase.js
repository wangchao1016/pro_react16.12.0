let httpBase = ''
const isNodeDev = process.env.NODE_ENV === 'development'  //判断运行环境

// .env.production文件，切换打包地址
const isBuildDev = process.env.REACT_APP_BUILD !== 'production'  //判断打包环境；development：打包到测试环境；production：打包正式环境

console.todo = function(msg) {
	console.log( '%c %s %s %s', 'color: yellow; background-color: #000;', '–', msg, '– ');
}
// 开发
if(isNodeDev){
  console.todo('dev环境')
  httpBase = ''

}else{
    if(isBuildDev){
        httpBase = 'https://development:8093'   //线上测试地址
        console.todo('测试环境build')
    }else{
        httpBase = 'https://production:8000'   //线上正式地址
        console.todo('正式环境build')
    }
}
// console.log('httpBase:', httpBase)

export default httpBase