import { combineReducers } from 'redux-immutable';  //通过react-immutable中的combineReducers，将store中的 state都生成immutable对象
import { reducer as headerReducer } from '../common/header/store'
import { reducer as homeReducer } from '../pages/home/store';

 //通过redux-immutable中的combineReducers将store中根state生成为一个immutable对象
const reducer = combineReducers({
    header: headerReducer,
    home: homeReducer   //home 即在store中state.home；存储的是home组件下的reducer
})

export default reducer