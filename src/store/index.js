import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducer';

//需要chrome中redux插件，必须要传这个参数，意思是window下是否有这个redux devtools 插件，如果有则调动window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()方法
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//一个react项目只允许有一个store，不允许出现多个，但是允许出现多个reducer
const store = createStore(reducer, composeEnhancers(
    applyMiddleware(thunk)  // applyMiddleware() 放的是中间件，它是redux的中间件，里面可以放一个或者多个，多个就是数组格式，在创建redux时候用到
))

export default store