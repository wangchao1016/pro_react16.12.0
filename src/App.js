import React from 'react';
import { BrowserRouter, Route  } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import { IconfontStyle } from './statics/iconfont/iconfont';
// import logo from './logo.svg';
import Header from './common/header';
import Home from './pages/home';
import httpBase from './httpBase';
console.log(httpBase)

function App() {
  return (
    // 类似于context中的provider,生产和消费，嵌套到provider里面的组件消费provider产生的数据
    // 因此Provider组件里面的组件都可以消费store中的state,但是需要在相应的组件中用connect()方法做链接
    <Provider store={store}>
      <IconfontStyle />
      <BrowserRouter>
        <div>
          <Header />
          <Route path="/" component={Home}></Route>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
