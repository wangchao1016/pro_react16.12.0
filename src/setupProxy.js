// 配置跨域代理
const proxy = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(proxy('/test', { 
       target: "https://h5api.zhefengle.cn",
       changeOrigin: true,
       secure: false,
       pathRewrite: {
        "^/test": "/"
       }
    })
  );
};