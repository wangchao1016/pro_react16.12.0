import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Topic from './components/Topic';
import List from './components/List';
import Recommend from './components/Recommend';
import Writer from './components/Writer';
import { actionCreators } from './store';

import { 
	HomeWrapper,
	HomeLeft,
    HomeRight,
    BackTop
} from './style';

class Home extends PureComponent {

	handleScrollTop() {
		window.scrollTo(0, 0);
	}

	render() {
		return (
			<HomeWrapper>
				<HomeLeft>
					<img className='banner-img' alt='' src="//upload.jianshu.io/admin_banners/web_images/4318/60781ff21df1d1b03f5f8459e4a1983c009175a5.jpg?imageMogr2/auto-orient/strip|imageView2/1/w/1250/h/540" />
					<Topic />
					<List />
				</HomeLeft>
				<HomeRight>
					<Recommend />
					<Writer />
				</HomeRight>
				{ this.props.showScroll ? <BackTop onClick={this.handleScrollTop}>顶部</BackTop> : null}
			</HomeWrapper>
		)
	}

	componentDidMount() {
		this.props.changeHomeData();
		this.bindEvents();
		
		this.props.test()
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.props.changeScrollTopShow);
	}

	bindEvents() {
		window.addEventListener('scroll', this.props.changeScrollTopShow);
	}

}
// 这个函数是用来定义，Counter需要store上哪个数据，放进去的数据是需要connect连接的
// mapStateToProps就是将store中的state映射到组件的props中
const mapState = (state) => ({ //state = store.getState()；这个state参数就是store里面的state
    showScroll: state.getIn(['home', 'showScroll'])  //将home.showScroll这个属性挂载到props中
    //这里之所以不能绑定state上面所有的属性，是考虑到性能开销，因为一旦state中任何节点属性变了，那么就会触发Counter这个组件重新渲染，这个是因为React的更新机制，一旦state变化，就会重新渲染该组件；这里只绑定count，是因为只有count发生变化，才触发组件重新渲染；因此必须把绑定的节点放到最小的范围
})

// mapDispatchToProps就是将dispatch和actionCreators映射到组件的props中
const mapDispatch = (dispatch) => ({ //这个函数是用来绑定dispatch()方法，将action直接当属性直接挂载到props中
	changeHomeData() {
		dispatch(actionCreators.getHomeInfo());
	},
	changeScrollTopShow() {
		if (document.documentElement.scrollTop > 100) {
			dispatch(actionCreators.toggleTopShow(true))
		}else {
			dispatch(actionCreators.toggleTopShow(false))
		}
	},
	test() { //测试跨域请求
		dispatch(actionCreators.login())
	}
});

// 当使用connect链接store与组件时，就不需要subscribe()订阅了，因为connect已经把给我们做了数据的映射
//mapStateToProps代表将state上的属性挂载到props;mapDispatchToProps将dispatch上的action挂载到props上，
// 以下表示将mapState，mapDispatch挂载的props放到Home组件中
export default connect(mapState, mapDispatch)(Home);
